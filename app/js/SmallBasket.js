class SmallBasket {
    constructor(productList) {
        this._productList = productList;
        this._btnArr = document.querySelectorAll('.main-content__card-button');
        this._basketTitle = document.querySelector('#basket-title');
        this._productListID = [];
        this._price = 0;
    }

    getProduct = (e) => {
        e.preventDefault();
        let productId = e.currentTarget.parentNode.children[1].getAttribute('data-id');
        console.log(productId);
        this._productListID.push(Number(productId));
        this._price += this._productList[productId].price;
        this.setBasketTitle();
    }

    getCountProduct = () => {
        return this._productListID.length;
    }

    setBasketTitle = () => {
        let count = this.getCountProduct;
        console.log(count());
        console.log(this._price);
        this._basketTitle.innerText = `Корзина (${count()})
                                        (${this._price} ₽)`;
    }

    showInfo = () => {
        this._btnArr.forEach((el) => el.addEventListener('click', (e) => {
            this.getProduct(e);
        }))
    }
}

export default SmallBasket;