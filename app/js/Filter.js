class Filter {
    constructor(productList) {
        this._productList = productList;
        this._filter = document.querySelector('.main-content__filter-section');
        this._checkbox = document.querySelectorAll('input');
        this._number = document.querySelectorAll('input[type=number]');
        this._popup = document.querySelector('.filter-result');
    }

    /*popup = () => {
        this._checkbox.forEach((item) =>
            item.addEventListener('change', this.draw)
        );
    }*/

    filter = () => {
        let types = [];
        let characters = [];
        let notChecked = this._productList;
        this._checkbox.forEach((item) => {
            if(item.checked && item.name === 'type') {
                types.push(item.value);
            }
            else if(item.checked && item.name === 'character') {
                characters.push(item.value);
            }
        })

        if (types.length > 0) {
            this._productList = this._productList.filter((item) => {
                return types.includes(item.type);
            });
        }
        if (characters.length > 0) {
            this._productList = this._productList.filter((item) => {
                return characters.includes(item.character);
            });
        }
        if (characters.length === 0 && types.length === 0) {
            this._productList = notChecked;
        }
        //this.popup();
        console.log('FOUND: ', this._productList.length);
    }

    showInfo = () => {
        console.log('SHOW INFO FILTER');
        this._checkbox.forEach((item) => {
            item.addEventListener('change', this.filter)
        })
    }
}

export default Filter;