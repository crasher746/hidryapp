const $template = document.querySelector('#template');
const $itemCard = document.querySelector('.main-content__cards-list');

export function toRender(data) {
    if (data.length < 1) return 0;
    for (let i = 0; i < data.length; i++) {
        let newCard = createCard(data[i]);
        $itemCard.appendChild(newCard);
    }
}

 function createCard(card) {
    let block = document.createElement('div');
    block.classList.add('main-content__card-item');
    block.append($template.content.cloneNode(true));
    block.querySelector('.main-content__card-push-image').src = `${card.image}`;
    block.querySelector('.main-content__card-title').textContent = `${card.name}`;
    block.querySelector('.main-content__card-price').textContent = `${card.price}`;
    block.querySelector('.main-content__card-price').dataset.id = `${card.id}`;
    return block;
}