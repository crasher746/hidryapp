import SmallBasket from "./SmallBasket.js";
import {toRender} from "./CatalogItems.js";
import {backlightningOn, backlightningOff} from "./Backlighting.js";
import Filter from "./Filter.js";

const $bonusOn = document.getElementById('bonus');
const $bonusOff = document.getElementById('bonus-off');

$bonusOn.addEventListener('click', backlightningOn);
$bonusOff.addEventListener('click', backlightningOff);

function httpGet(url) {
    return new Promise(function (resolve, reject) {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onload = function () {
            if (this.status === 200) {
                let data = JSON.parse(this.responseText);
                toRender(data);
                let smallBasket = new SmallBasket(data);
                smallBasket.showInfo();
                resolve(this.response);
                let filter = new Filter(data);
                filter.showInfo();
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        };

        xhr.send();
    });
}

httpGet("../api/cards.json").then(
    response => console.log(`Fulfilled: ${response}`),
    error => console.log(`Rejected: ${error}`)
);