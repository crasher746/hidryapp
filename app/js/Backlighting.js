const $elements = document.getElementsByTagName('div');
const $style = document.createElement('style');
const $head = document.head;

function getRandomElement (elements) {
    let result = elements[Math.floor(Math.random() * elements.length)];
    return result;
}

function getRandomColor () {
    const symbols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += symbols[Math.floor(Math.random() * symbols.length)];
    }
    return color;
}

function setColor (element, index) {
    $style.innerHTML += `.set-new-color-${index} {background: ${getRandomColor()};}`;
    $head.append($style);
    element.classList.add(`set-new-color-${index}`);
}

function firstTask () {
    window.onload = function () {
        let element = getRandomElement($elements);
        setColor(element, 100);
    }
}

let counter = 1;
let colorInterval;
let removeInterval;
function secondTask () {
    colorInterval = setInterval(function () {
        let element = getRandomElement($elements);
        counter = counter + 1;
        setColor(element, counter);
    }, 2000);
}

function removeClass (element, index) {
    element.classList.remove(`set-new-color-${index}`);
}

function thirdTask () {
    removeInterval = setInterval(function () {
        let starsWith = 'set-new-color-';
        let elementWithClass = document.querySelectorAll(".set-new-color-");
        let element = getRandomElement();
        let index = Math.floor(Math.random() * counter);

        let elClasses = element.className.split(" ").filter(function (string) {
            return string.indexOf(starsWith, 0) !== 0;
        });
        console.log(elClasses);
        element.className = elClasses.join(" ");
    }, 2000);
}

export function backlightningOn() {
    firstTask();
    secondTask();
    thirdTask();
}

export function backlightningOff() {
    let starsWith = 'set-new-color-';

    clearInterval(colorInterval);
    clearInterval(removeInterval);

    for (let i = 0; i < $elements.length; i++) {
        let elClasses = $elements[i].className.split(" ").filter(function (string) {
            return string.indexOf(starsWith, 0) !== 0;
        });
        $elements[i].className = elClasses.join(" ");
    }
}
